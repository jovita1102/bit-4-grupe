package Masyvai;

public class MinMAxRykiavimas {
    public static void main(String[] args) {
        Integer[] skaiciai = {1, 3, 2};

        int pos_min, temp;

        for (int i = 0; i < skaiciai.length - 1; i++) {
            pos_min = i;//set pos_min to the current index of array

            for (int j = i + 1; j < skaiciai.length; j++) {
                if (skaiciai[j] < skaiciai[pos_min]) {
                    //pos_min will keep track of the index that min is in, this is needed when a swap happens
                    pos_min = j;
                }
            }

            //if pos_min no longer equals i than a smaller value must have been found, so a swap must occur
            if (pos_min != i) {
                temp = skaiciai[i];
                skaiciai[i] = skaiciai[pos_min];
                skaiciai[pos_min] = temp;
            }


        }
        for (Integer skaicius: skaiciai)
            System.out.println(skaicius);
    }
}
