package Masyvai;

import java.sql.Array;
import java.sql.SQLOutput;
import java.util.Arrays;

public class Reverse {
    public static void main(String[] args) {
        int[] a = {2, 4, 6, 8, 12};
        reverse(a);
    }

    private static void reverse(int[] a) {
        Integer[] b = new Integer[a.length];
        Integer j = 0;
        for (int i = a.length - 1; i >= 0; i--) {
            b[j] = a[i];
            j++;
        }
        System.out.println(Arrays.toString(a));
        System.out.println(Arrays.toString(b));
    }
}
