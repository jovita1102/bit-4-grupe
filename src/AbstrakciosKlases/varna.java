package AbstrakciosKlases;

public class varna extends Paukstis {

    @Override
    public String gautiPavadinima() {
        return "varna";
    }

    @Override
    public Integer gautiGyvenimoAmziu() {
        return  5;
    }

    @Override
    public Double gautiSvori() {
        return 3.0;
    }
}
