package Ciklai;

import java.util.Scanner;

public class SwichDienos {
    public static void main(String[] args) {
        Scanner skaitytuvas = new Scanner(System.in);
        System.out.println("Iveskite skaiciu nuo 1 iki 7");
        Integer savaitesDiena = skaitytuvas.nextInt();

        switch (savaitesDiena) {
            case 1:
                System.out.println("Pirmadienis");
                break;
            case 2:
                System.out.println("Antradienis");
                break;
            case 3:
                System.out.println("Treciadienis");
                break;
            case 4:
                System.out.println("Ketvirtadienis");
                break;
            case 5:
                System.out.println("Penktadienis");
                break;
            case 6:
                System.out.println("Sestadienis");
                break;
            case 7:
                System.out.println("Sekmadienis");
                break;
            default:
                System.out.println("Perskaityk daar karta uzduoti");

        }
    }
}


