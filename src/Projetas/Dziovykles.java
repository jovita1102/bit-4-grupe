package Projetas;

public class Dziovykles extends Preke {
      private String dziovyklesTalpa;

    public Dziovykles(Integer id, String prekesTipas, String prekesPavadinimas, Integer prekesKiekis, Double prekesKaina, String dziovyklesTalpa) {
        super(id, prekesTipas, prekesPavadinimas, prekesKiekis, prekesKaina);
        this.dziovyklesTalpa = dziovyklesTalpa;
    }

    public String getDziovyklesTalpa() {
        return dziovyklesTalpa;
    }

    public void setDziovyklesTalpa(String dziovyklesTalpa) {
        this.dziovyklesTalpa = dziovyklesTalpa;
    }
}
