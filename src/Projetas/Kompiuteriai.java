package Projetas;

public class Kompiuteriai extends Preke {
        private String kompoProcesorius;
        private String kompoRam;
        private String kompoDiskoTalpa;

    public Kompiuteriai(Integer id, String prekesTipas, String prekesPavadinimas, Integer prekesKiekis, Double prekesKaina, String kompoProcesorius, String kompoRam, String kompoDiskoTalpa) {
        super(id, prekesTipas, prekesPavadinimas, prekesKiekis, prekesKaina);
        this.kompoProcesorius = kompoProcesorius;
        this.kompoRam = kompoRam;
        this.kompoDiskoTalpa = kompoDiskoTalpa;
    }

    public String getKompoProcesorius() {
        return kompoProcesorius;
    }

    public void setKompoProcesorius(String kompoProcesorius) {
        this.kompoProcesorius = kompoProcesorius;
    }

    public String getKompoRam() {
        return kompoRam;
    }

    public void setKompoRam(String kompoRam) {
        this.kompoRam = kompoRam;
    }

    public String getKompoDiskoTalpa() {
        return kompoDiskoTalpa;
    }

    public void setKompoDiskoTalpa(String kompoDiskoTalpa) {
        this.kompoDiskoTalpa = kompoDiskoTalpa;
    }
}
