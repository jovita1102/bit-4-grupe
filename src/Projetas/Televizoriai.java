package Projetas;

public class Televizoriai extends Preke {
    private String televizoriausTechnologija;
    private String televizoriausraiska;

    public Televizoriai(Integer id, String prekesTipas, String prekesPavadinimas, Integer prekesKiekis,
                        Double prekesKaina, String televizoriausTechnologija, String televizoriausraiska) {
        super(id, prekesTipas, prekesPavadinimas, prekesKiekis, prekesKaina);
        this.televizoriausTechnologija = televizoriausTechnologija;
        this.televizoriausraiska = televizoriausraiska;
    }

    public String getTelevizoriausTechnologija() {
        return televizoriausTechnologija;
    }

    public void setTelevizoriausTechnologija(String televizoriausTechnologija) {
        this.televizoriausTechnologija = televizoriausTechnologija;
    }

    public String getTelevizoriausraiska() {
        return televizoriausraiska;
    }

    public void setTelevizoriausraiska(String televizoriausraiska) {
        this.televizoriausraiska = televizoriausraiska;
    }

    @Override
    public String toString() {
        return "Televizoriai{" +
                "televizoriausTechnologija='" + televizoriausTechnologija + '\'' +
                ", televizoriausraiska='" + televizoriausraiska + '\'' +
                '}';
    }
}