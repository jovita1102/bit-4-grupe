package Projetas;

class Preke {
    private Integer id;
    private String prekesTipas;
    private String prekesPavadinimas;
    private Integer prekesKiekis;
    private Double prekesKaina;

    public Preke() {

    }

    public Preke(Integer id, String prekesTipas, String prekesPavadinimas, Integer prekesKiekis, Double prekesKaina) {
        this.id = id;
        this.prekesTipas = prekesTipas;
        this.prekesPavadinimas = prekesPavadinimas;
        this.prekesKiekis = prekesKiekis;
        this.prekesKaina = prekesKaina;

    }


    public String toString() {
        return "Preke{" +
                "Prekes id='" + id + '\'' +
                ", Prekes tipas='" + prekesTipas + '\'' +
                ", Prekes pavadinimas=" + prekesPavadinimas +
                ", Prekes kiekis=" + prekesKiekis +
                ", Prekes kaina=" + prekesKaina +
                '}' + "\n";
    }

    public Integer getIdd() {
        return id;
    }


    public String getPrekesTipas() {
        return prekesTipas;
    }

    public String getPrekesPavadinimas() {
        return prekesPavadinimas;
    }

    public Integer getPrekesKiekis() {
        return prekesKiekis;
    }

    public Double getPrekesKaina() {
        return prekesKaina;
    }


    public void setId(Integer id) {
        this.id = id;
    }

    public void setPrekesTipas(String prekesTipas) {
        this.prekesTipas = prekesTipas;
    }

    public void setPrekesPavadinimas(String prekesPavadinimas) {
        this.prekesPavadinimas = prekesPavadinimas;
    }

    public void setPrekesKiekis(Integer prekesKiekis) {
        this.prekesKiekis = prekesKiekis;
    }

    public void setPrekesKaina(Double prekesKaina) {
        this.prekesKaina = prekesKaina;
    }
}
