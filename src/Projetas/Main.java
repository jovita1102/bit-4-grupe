package Projetas;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main<variantas> {


    public static void main(String[] args) {

        String duomenys = new File("").getAbsolutePath()
                + "/src/Projetas/prekes.txt";

        List<Preke> prekes = new ArrayList<>();


        String rezultatuFailas = new File("").getAbsolutePath()
                + "/src/Projetas/atfiltruotosPrekes.txt";
        skaitymas(duomenys, prekes);
        System.out.println(prekes);
        System.out.println("Iveskite skaiciu nuo 1 iki 6");
        meniu(prekes);

        maziausiaiPrekiu(prekes);
        daugiausiaPrekiu(prekes);
        filtruotiPagalTipa(prekes);

        List<Preke> pagalTipa = filtruotiPagalTipa(prekes);
        rasyti(rezultatuFailas, pagalTipa);
        rasyti(duomenys, prekes);
    }

    public static void meniu(List<Preke> prekes) {
        Scanner scanner = new Scanner(System.in);
        int listas = scanner.nextInt();

        switch (listas) {
            case 1:
                System.out.println("Rasti prekę, kurios vienetų turi mažiausiai " + maziausiaiPrekiu(prekes));
                listas = scanner.nextInt();
            case 2:
                System.out.println("Rasti prekę, kurios vienetų turi daugiausiai " + daugiausiaPrekiu(prekes));
                listas = scanner.nextInt();
            case 3:

                System.out.println("Išfiltruoti prekes pagal tipą " + filtruotiPagalTipa(prekes));
                listas = scanner.nextInt();
            case 4:
                System.out.println(" Ištrinti prekę pagal prekės id ");

                System.out.println(prekes);
                listas = scanner.nextInt();
            default:
                System.out.println(prekes);
        }
    }

    public static void skaitymas(String duomenys,
                                 List<Preke> prekes) {

        try (BufferedReader skaitytuvas = new BufferedReader(new FileReader(duomenys))) {
            String eilute = skaitytuvas.readLine();
            while (eilute != null) {
                String[] eilutesDuomenys = eilute.split(" ");
                Integer id = Integer.parseInt(eilutesDuomenys[0]);
                String prekesTipas = eilutesDuomenys[1];
                String prekesPavadinimas = eilutesDuomenys[2];
                Integer prekesKiekis = Integer.parseInt(eilutesDuomenys[3]);
                Double prekesKaina = Double.parseDouble(eilutesDuomenys[4]);

                Preke objektas;
                if (eilutesDuomenys[1].equals("televizoriai")) {
                    String televizoriausTechnologija = eilutesDuomenys[5];
                    String televizoriausRaiska = eilutesDuomenys[6];
                    objektas = new Televizoriai(id, prekesTipas, prekesPavadinimas,
                            prekesKiekis, prekesKaina,
                            televizoriausTechnologija, televizoriausRaiska);

                } else if (eilutesDuomenys[1].equals("nesiojamas-kompiuteris")) {
                    String kompoProcesorius = eilutesDuomenys[5];
                    String kompoRam = eilutesDuomenys[6];
                    String kompoDiskoTalpa = eilutesDuomenys[7];
                    objektas = new Kompiuteriai(id, prekesTipas, prekesPavadinimas,
                            prekesKiekis, prekesKaina,
                            kompoProcesorius, kompoDiskoTalpa, kompoRam);
                } else if (eilutesDuomenys[1].equals("skalbykle")) {
                    String skalbyklesTalpa = eilutesDuomenys[5];
                    objektas = new Skalbykles(id, prekesTipas, prekesPavadinimas,
                            prekesKiekis, prekesKaina,
                            skalbyklesTalpa);
                } else {
                    String dziovyklesTalpa = eilutesDuomenys[5];
                    objektas = new Dziovykles(id, prekesTipas, prekesPavadinimas,
                            prekesKiekis, prekesKaina,
                            dziovyklesTalpa);
                }

                prekes.add(objektas);

                eilute = skaitytuvas.readLine();
            }

        } catch (FileNotFoundException ex) {
            System.out.println("Failas nerastas");
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public static List<Preke> maziausiaiPrekiu(List<Preke> prekes) {
        List<Preke> min = new ArrayList<>();
        Integer maziausiasKiekis = 50;
        Integer objektas = 0;

        for (int i = 0; i < prekes.size(); i++) {
            if (prekes.get(i).getPrekesKiekis() < maziausiasKiekis) {
                maziausiasKiekis = prekes.get(i).getPrekesKiekis();
                objektas = i;

            }
        }
        min.add(prekes.get(objektas));
        return min;
    }

    public static List<Preke> daugiausiaPrekiu(List<Preke> prekes) {
        List<Preke> daugiausia = new ArrayList<>();
        Integer maksimalusKiekis = 0;
        Integer obj = 0;

        for (int i = 0; i < prekes.size(); i++) {
            if (prekes.get(i).getPrekesKiekis() > maksimalusKiekis) {
                maksimalusKiekis = prekes.get(i).getPrekesKiekis();
                obj = i;

            }
        }
        daugiausia.add(prekes.get(obj));
        return daugiausia;
    }

    public static List<Preke> filtruotiPagalTipa(List<Preke> prekes) {
        List<Preke> prekesPagalTipa = new ArrayList<>();
        for (int i = 0; i < prekes.size(); i++) {
            if (prekes.get(i).getPrekesTipas().equals("televizoriai")) {
                prekesPagalTipa.add(prekes.get(i));
            }
        }
        return prekesPagalTipa;
    }

    public static void rasyti(String atrinktosFailas, List<Preke> pagalTipa) {
        try (BufferedWriter spausdinimas = new BufferedWriter(new FileWriter(atrinktosFailas))) {
            spausdinimas.write("Atrinktos prekes:\n");
            for (Preke preke : pagalTipa) {
                spausdinimas.write(preke.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static List<Preke> pridetiNaujaPreke(List<String> prekes, String duomenys) {
        Scanner skaitytuvas = new Scanner(System.in);
        System.out.println("Prideti nauja preke:1 - skalbykle, 2 - Dziovykle, 3 - kompiuteris, 4 - televizorius");
        Integer pasirinkimas = skaitytuvas.nextInt();
        String prTipas;
        System.out.println("Prekes pavadinimas");
        String prPavadinimas = skaitytuvas.next();
        System.out.println("Prekes kiekis");
        Integer kiekis = skaitytuvas.nextInt();
        System.out.println("Prekes kaina");
        Double prKaina = skaitytuvas.nextDouble();
        if (pasirinkimas == 1) {
            prTipas = "skalbykle";
            System.out.println("Skalbykles talpa");
            String skTalpa = skaitytuvas.next();
            Preke naujaSkalbykle = new Skalbykles(1, prTipas, prPavadinimas, kiekis, prKaina, skTalpa);
            prekes.add(naujaSkalbykle);
            rasyti(duomenys, prekes);


        } else if (pasirinkimas == 2) {
            prTipas = "dziovykle";
            System.out.println("Dziovykles talpa");
            String dzTalpa = skaitytuvas.next();
            Preke naujaDziovykle = new Dziovykles(6, prTipas, prPavadinimas, kiekis, prKaina, dzTalpa);
            prekes.add(naujaDziovykle);
            rasyti(duomenys, prekes);

        } else if (pasirinkimas == 3) {
            prTipas = "kompiuteris";
            System.out.println(" Kompiuterio procesorius");
            String procesorius = skaitytuvas.next();
            System.out.println("Kiek RAM");
            String kompoRam = skaitytuvas.next();
            System.out.println("Disko talpa");
            String diskoTalpa = skaitytuvas.next();
            Preke naujasKompiuteris = new Kompiuteriai(3, prTipas, prPavadinimas, kiekis, prKaina, procesorius, kompoRam, diskoTalpa);
            prekes.add(naujasKompiuteris);
            rasyti(duomenys, prekes);


        } else if (pasirinkimas == 4) {
            prTipas = "televizorius";
            System.out.println("TV technologija");
            String tvTechnologija = skaitytuvas.next();
            System.out.println("TV raiska");
            String tvRaiska = skaitytuvas.next();
            Preke naujasTv = new Televizoriai(4, prTipas, prPavadinimas, kiekis, prKaina, tvTechnologija, tvRaiska);
            prekes.add(naujasTv);
            rasyti(duomenys, prekes);

        }

        public static void istrintaPreke (List<Preke>prekes, Integer id){
            for (int i = 0; i < prekes.size(); i++) {
                if (prekes.get(i).getPrekeid().equals(id)) {
                    prekes.remove(prekes.get(i));

                }


            }

            public static void istrintosPrekesIrasymas (String Id, List < Preke > istrintaPreke){
                try (BufferedWriter spausdinimas = new BufferedWriter(new FileWriter(Id))) {
                    for (Preke preke : istrintaPreke) {
                        spausdinimas.write(preke.toString());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


        }
    }
}









