package Projetas;

public class Skalbykles extends Preke {
        private String skalbyklesTalpa;

    public Skalbykles(Integer id, String prekesTipas, String prekesPavadinimas, Integer prekesKiekis, Double prekesKaina, String skalbyklesTalpa) {
        super(id, prekesTipas, prekesPavadinimas, prekesKiekis, prekesKaina);
        this.skalbyklesTalpa = skalbyklesTalpa;
    }
}
