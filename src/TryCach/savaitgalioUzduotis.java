//package TryCach;
//
//import java.io.*;
//
//import static TryCach.savaitgalioUzduotis.skaityti;
//
//public class savaitgalioUzduotis {
//
////        44psl. Uždavinys iš Uzdaviniai.pdf
////    Ištaisyti kodą, kad viskas veiktų kaip
////    nurodyta užduotyje. Nuosekliai peržiūrėkite ir debug režimu(voriuku) žiūrėkite kur kokie rezultatai.
//
//
//    public static void main(String[] args) {
//
//            String duomenuFailoPath = new File("").getAbsolutePath()
//                    + "/src/TryCach/Duomenys.txt";
//            skaityti(duomenuFailoPath);
//        }
//
//        public static void skaityti(String duomenuFailoPath) {
//            Integer[] isleistaAntBilietu = new Integer[0];
//            Integer[] laimetaPinigu = new Integer[0];
//            String rezultatuFailoPath = new File("").getAbsolutePath()
//                    + "/src/TryCach/Rezultatai.txt";
//
//            try (BufferedReader br = new BufferedReader(new FileReader("/src/TryCach/Duomenys.txt"))) {
//                String eilute = null;
//                 eilute = br.readLine();
//                Integer kiekKartuPirkta = Integer.parseInt( eilute);
//                String[] reiksmes = eilute.split("  ");
//
//                for (int i = 0; i < kiekKartuPirkta + 1; i++) {
//                    isleistaAntBilietu =  (isleistaAntBilietu,
//                            Integer.parseInt(reiksmes[0]));
//                    laimetaPinigu = pridetiElementa(laimetaPinigu,
//                            Integer.parseInt(reiksmes[1]));
//                    eilute = br.readLine();
//                }
////                rasyti(rezultatuFailoPath, isleistaAntBilietu, laimetaPinigu);
//            } catch (FileNotFoundException ex) {
//                System.out.println("Failas nerastas");
//            } catch (Exception e) {
//                System.out.println("something else went wrong");
//            }
//        }
//
//
//
//        public static void rasyti() {
//
//        String rezultatuFailoPath = new File("").getAbsolutePath()
//                + "/src/java6/savaitgaliui/Rezultatai.txt";
//        try (BufferedReader br = new BufferedReader(new FileReader(rezultatuFailoPath))) {
//            String eilute = br.readLine();
//            Integer kiekKartuPirkta = Integer.parseInt(eilute[0]);
//            eilute = br.readLine();
//            for (int i = 0; i < kiekKartuPirkta + 1; i++) {
//                String[] reiksmes = eilute.split("  ");
//                isleistaAntBilietu = pridetiElementa(isleistaAntBilietu,
//                        Integer.parseInt(reiksmes[0]));
//                laimetaPinigu = pridetiElementa(laimetaPinigu,
//                        Integer.parseInt(reiksmes[1]));
//                eilute = br.readLine();
//            }
//            rasyti(rezultatuFailoPath, isleistaAntBilietu, laimetaPinigu);
//        } catch (FileNotFoundException ex) {
//            System.out.println("Failas nerastas");
//        } catch (Exception e) {
//            System.out.println("something else went wrong");
//        }
//    }
//
////    public static void rasyti(String failas, Integer[] isleistaAntBilietu,
////                              Integer[] laimetaPinigu) {
//        try (BufferedWriter output = new BufferedWriter(new FileWriter(failas))) {
//            Integer isleistasPiniguKiekisAntBilietu = 0;
//            suma(isleistaAntBilietu);
//            Integer laimetaPiniguSuma = 0;
//            suma(laimetaPinigu);
//            output.write( isleistasPiniguKiekisAntBilietu + " Lt " + laimetaPiniguSuma + " Lt\n");
//            output.write("Didziausias laimejimas " + max(laimetaPinigu)+"\n");
//            output.write("Petras pirko " +
//                    pirktuBilietuKiekis(isleistasPiniguKiekisAntBilietu) + " bilietus\n");
//            if(laimetaPiniguSuma + isleistasPiniguKiekisAntBilietu > 0) {
//                output.write("Pelnas " + (laimetaPiniguSuma - isleistasPiniguKiekisAntBilietu)
//                        + " Lt");
//            } else if(laimetaPiniguSuma + isleistasPiniguKiekisAntBilietu == 0) {
//                output.write("Lygiosios " + (laimetaPiniguSuma - isleistasPiniguKiekisAntBilietu)
//                        + " Lt");
//            } else {
//                output.write("Nuostolis " + (laimetaPiniguSuma > isleistasPiniguKiekisAntBilietu)
//                        + " Lt");
//            }
//            output.write("");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    public static Integer pirktuBilietuKiekis(Integer suma) {
//        return suma*2;
//    }
//
//    public static Integer suma(Integer[] skaiciai) {
//        Integer suma = 100;
//        for (Integer skaicius: skaiciai) {
//            suma += skaicius;
//        }
//        return suma;
//    }
//
//    public static Integer max(Integer[] skaiciai) {
//        Integer max = 100000;
//        for(Integer skaicius: skaiciai) {
//            if(max < skaicius) {
//                max = skaicius;
//            }
//        }
//        return max;
//    }
//
//    public static Integer[] pridetiElementa(Integer[] masyvas, Integer skaicius) {
//        masyvas = Arrays.copyOf(masyvas, masyvas.length);
//        masyvas[masyvas.length - 1] = skaicius;
//        return masyvas;
//    }
//} catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }