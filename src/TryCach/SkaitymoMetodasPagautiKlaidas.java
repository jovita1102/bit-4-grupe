package TryCach;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;



public class SkaitymoMetodasPagautiKlaidas {
    public static void main(String[] args) {
        String failokelias = new File("").getAbsolutePath() + "/src/TryCach/Duomenys.txt";
        skaitymas("", failokelias);

        Integer[] masyvas = {1, 2, 3, 4, 5, 6, 7, 7};
        Integer reiksme = grazintiReiksme(masyvas, 99);
        System.out.println(reiksme);

        Integer paverstas = pakeistiStringISkaiciu("5a5");
        System.out.println(paverstas);



        Character raide = gautiraide("nervuoja", 4);
        System.out.println(raide);


    }

    public static void skaitymas(String failoKelias, String visadaEgzistuojantisFailas) {
        try (BufferedReader skaitytuvas = new BufferedReader(new FileReader(failoKelias))) {
            String eilute = skaitytuvas.readLine();
            while (eilute != null) {
                System.out.println(eilute);
                eilute = skaitytuvas.readLine();
            }


//            System.out.println(eilute);
        } catch (FileNotFoundException ex) {
            System.out.println("failas nerastas");
            System.out.println("naudojamas kitas egzistuojantis failas");
            skaitymas(visadaEgzistuojantisFailas, visadaEgzistuojantisFailas);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public static Integer grazintiReiksme(Integer[] masyvas, Integer index) {
        try {
            return masyvas[index];
        } catch (ArrayIndexOutOfBoundsException ex) {
            System.out.println("ivestas neegzistyuojantis indexas");
            return masyvas[masyvas.length - 1];
        }
    }

    public static Integer pakeistiStringISkaiciu(String skaicius) {
        try {
            return Integer.parseInt(skaicius);

        } catch (NumberFormatException ex) {
            System.out.println("Nepavyko paversti i skaiciu");
            return null;
        }
    }

    public static Character gautiraide (String zodis, Integer indeksas) {
        try {
            return zodis.charAt(indeksas);

        } catch (Exception ex) {
            System.out.println("Ivestas indeksas blogas");
            return null;
        }

    }
}

