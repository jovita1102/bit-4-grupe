package FunkcijosMetodai;

public class Funkcijos {
    public static void main(String[] args) {
     Integer[] skaiciai = {1, 2, 3, 4, 5};
        spausdintiMasyva(skaiciai, "Spausdinu pirma masyva");
       Integer[] skaiciai2 = {9,9,9,9,9};
       keistiMasyva(skaiciai2);
        spausdintiMasyva(skaiciai2, "Spausdinu antra masyva");
       Integer suma = dviejuSkaiciuSuma(2,2);
       System.out.println(suma);
        spausdinti(1, 2, 3, 4, 9, 22);
        int a= 10;
        kitas (a);
        System.out.println(a);

    }
    public static void keistiMasyva (Integer[] masyvas){

        masyvas[1]=99999;
    }
    public static void kitas (int a){
        a=200;
        System.out.println(a);
    }
//  metodas
    public static void spausdintiMasyva (Integer[] betkas, String pavadinimas ){
        System.out.println(pavadinimas);
        for (Integer skaicius :betkas){
            System.out.println(skaicius);
        }
    }
// funkcijos
    public static Integer dviejuSkaiciuSuma (Integer a, Integer b){
        return a+b;

    }
    public static void spausdinti (Integer...skaiciai) {
        System.out.println(skaiciai[0]);
        System.out.println(skaiciai[1]);
        System.out.println(skaiciai);
    }
}
