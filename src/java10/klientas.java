package java10;

public class klientas implements mokejimas {
    private String bankoSaskaita;
    private String asmensVardas;
    private Double suma;

    public klientas() {

    }

    public String gautibankoSaskaita() {
        return bankoSaskaita;
    }

    public klientas(String bankoSaskaita, String asmensVardas, Double suma) {
        this.bankoSaskaita = bankoSaskaita;
        this.asmensVardas = asmensVardas;
        this.suma = suma;
    }

    public String toString() {
        return "banko sf:" + gautibankoSaskaita() +
                "asmens vardas" + gautiSaskaitosAsmensVarda() +
                "suma" + gautiSuma();
    }

    public String gautiSaskaitosAsmensVarda() {
        return asmensVardas;
    }

    public Double gautiSuma() {
        return suma;
    }
}
