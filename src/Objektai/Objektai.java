package Objektai;

import java.io.*;

public class Objektai {
    public static void main(String[] args) {

//        Darbuotojas obj = new Darbuotojas("Jonas", "Jonaitis", 15, "kasininkas");
//
//        System.out.println(obj);
//        obj.setVardas("Petras");
//        System.out.println(obj);
//        Darbuotojas obj2 = new Darbuotojas("Andrius", "Andrelis", 24);
//        System.out.println(obj2);
        String failokelias = new File("").getAbsolutePath() + "/src/TryCach/duomenys11.txt";
        Darbuotojas[] darbuotojas = skaitymas(failokelias);
        spausdinamDarbuotojus(darbuotojas);

    }
    public static void spausdinamDarbuotojus (Darbuotojas[] darbuotojas){
        for (int i = 0 ; i<darbuotojas.length; i++){
            System.out.println(darbuotojas[i]);
        }
    }

    public static  Darbuotojas [] skaitymas (String failoKelias) {
        Darbuotojas[] darbuotojai = new Darbuotojas[3];
        try (BufferedReader skaitytuvas = new BufferedReader(new FileReader(failoKelias))) {
            String eilute = skaitytuvas.readLine();
            Integer indeksas = 0;
            while (eilute != null) {
                String[] eilDuomenys = eilute.split(" ");
                String vardas = eilDuomenys[0];
                String pavarde = eilDuomenys[1];
                Integer amzius = Integer.parseInt(eilDuomenys[2]);
                String pareigos = eilDuomenys[3];
                Darbuotojas objektas = new Darbuotojas(vardas, pavarde, amzius, pareigos);
//                Idedam masyva

                darbuotojai [indeksas]= objektas;
                indeksas++;

                eilute = skaitytuvas.readLine();

            }
        } catch (FileNotFoundException ex) {
            System.out.println("failas nerastas");
        } catch (Exception e) {
            System.out.println(e);;
        }
        return darbuotojai;

    }


}