package java2;

import java.util.Scanner;

public class automobilis {
    public static void main(String[] args) {
        Scanner skaitytuvas = new Scanner(System.in);

        System.out.println("Kiek automobilis 100 km sunaudoja kuro?");
        double kuroSanaudos = skaitytuvas.nextDouble();

        System.out.println("Kiek km nuvaziuota?");
        double nuvaziuotiKm = skaitytuvas.nextDouble();

        System.out.println("Kiek zmoniu susirenge i kelione?");
        double zmoniuSkaicius = skaitytuvas.nextDouble();

        System.out.println("kiek kainuoja 1 litras degalu?");
        double kainaKuro = skaitytuvas.nextDouble();
        skaitytuvas.close();


        double kuroSuma = kuroSanaudos*nuvaziuotiKm;
        double pinigai = kuroSuma*kainaKuro;
        double pinigaiZmogui = (pinigai/zmoniuSkaicius)/100;
        String result = String.format("%.1f", pinigaiZmogui);
        System.out.println("Vienam zmogui kelione kainuos "+ result + " Litu");

    }
}
