package java2;

import java.util.Scanner;

public class StatusisTrikampis{

public static class Main{

    public static void main(String[] args) {
        Scanner skaitytuvas = new Scanner(System.in);

        System.out.println("Kokia trikampio krastine a");
        Integer a = skaitytuvas.nextInt();
        System.out.println("Kokia trikampio krastine b");
        Integer b = skaitytuvas.nextInt();

        skaitytuvas.close();

        Integer plotas = a*b/2;
        System.out.println("Trikampio plotas " + plotas);

    }
}
}