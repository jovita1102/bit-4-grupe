package java2;

import java.util.Scanner;

public class vandensSaugykla {
    public static void main(String[] args) {
        Scanner skaitytuvas = new Scanner(System.in);

        System.out.println("Kiek saugykloje yra kubiniu metru vandens?");
        float kubiniaiMetraiVandens = skaitytuvas.nextFloat();

        System.out.println("Kiek zmoniu vartoja vandeni?");
        float zmoniuSkaicius = skaitytuvas.nextFloat();

        System.out.println("Kiek per para vidutiniskai vienas zmogus sunaudoja kubiniu metru vandens?");
        float vandensSuvartojimasVieno = skaitytuvas.nextFloat();
        skaitytuvas.close();

        float vandensSuvartojimas = (zmoniuSkaicius*vandensSuvartojimasVieno);
        float rez = kubiniaiMetraiVandens/vandensSuvartojimas;
        System.out.println("Vandens uzteks " + rez + " paroms");

    }
}
