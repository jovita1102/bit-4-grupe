package java2;

import java.util.Scanner;

public class Geles {
    public static void main(String[] args) {
        Scanner skaitytuvas = new Scanner(System.in);

        System.out.println("Kiek geliu zydi?");
        Integer zydi = skaitytuvas.nextInt();

        System.out.println("Kiek geliu prazysta kiekviena diena?");
        Integer prazydo = skaitytuvas.nextInt();

        System.out.println("Kiek dienu praejo?");
        Integer dienos = skaitytuvas.nextInt();
        skaitytuvas.close();

        Integer rezultatas = zydi+(prazydo*dienos);
        System.out.println("Po " + dienos + " dienu  zydes "+ rezultatas+" geliu. " );
    }
}
