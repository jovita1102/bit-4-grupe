package java2;

import java.util.Scanner;

public class DidziausiasSkaicius {


    public static void main(String[] args) {
        Scanner skaitytuvas = new Scanner(System.in);
        System.out.println("Iveskite pirma skaiciu");
        double pirmasSkaicius = skaitytuvas.nextDouble();

        System.out.println("Iveskite antra skaiciu");
        double antrasSkaicius = skaitytuvas.nextDouble();

        System.out.println("Iveskite trecia skaiciu");
        double treciasSkaicius = skaitytuvas.nextDouble();

        skaitytuvas.close();

        if (pirmasSkaicius > antrasSkaicius && pirmasSkaicius >treciasSkaicius) {
            System.out.println("pirmas skaicius didziausias");
        } else if (antrasSkaicius > pirmasSkaicius && antrasSkaicius > treciasSkaicius){
            System.out.println("antras skaicius didziausias");
        } else {
            System.out.println("trecias skaicius didziausias");
        }
    }
}
