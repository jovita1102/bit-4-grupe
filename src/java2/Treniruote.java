package java2;

import java.util.Scanner;

public class Treniruote {
    public static void main(String[] args) {
        Scanner skaitytuvas = new Scanner(System.in);

        System.out.println("Kokia treniruotes trukme pirmadieni?");
        double pirmadienis = skaitytuvas.nextDouble();

        System.out.println("Kokia treniruotes trukme antradieni?");
        double antradienis = skaitytuvas.nextDouble();

        System.out.println("Kokia treniruotes tukme treciadieni?");
        double treciadienis = skaitytuvas.nextDouble();

        System.out.println("Kokia treniruotes trukme ketvirtadieni?");
        double ketvirtadienis = skaitytuvas.nextDouble();

        System.out.println("Kokia treniruotes trukme penktadieni?");
        double penktadienis = skaitytuvas.nextDouble();
        skaitytuvas.close();

        double laikas = (pirmadienis+antradienis+treciadienis+ketvirtadienis+penktadienis)*60;
        System.out.println("Per savaite Andrius treniruojasi " + laikas + " minuciu .");
    }
}
