package java12;

public class BendrineKlase<T> {

    private T objektas;

    public T get() {
        return objektas;
    }

    public void set(T objektas) {
        this.objektas = objektas;
    }
}
