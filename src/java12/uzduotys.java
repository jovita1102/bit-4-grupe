package java12;

public class uzduotys {
    public static void main(String[] args) {
        Double[] doublai={1.0, 1.2, 3.0, 4.0};
        Integer[] skaiciai= {1, 2, 3, 4};
        Character[] charai= {'a', 'b', 'c', 'd'};
        spausdinti(skaiciai);
        spausdinti(doublai);
        spausdinti(charai);
    }
    public static <T> void spausdinti(T[] masyvas){
        for(T reiksme: masyvas){
            System.out.println(reiksme+" ,");
        }
        System.out.println();
    }
}
